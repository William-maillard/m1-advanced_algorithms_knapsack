"""
This module contains functions to generate knapsack problems
Author: William 
"""
from argparse import Namespace
from enum import Enum
import random
import math

random.seed()

#some usefull constants to parametize the generator.
MAX_NUMBER_OF_ITEMS = 1000
OBJECT_CONSTRAINT_MAX = 1000
MAX_KNAPSACK_WEIGHT = MAX_NUMBER_OF_ITEMS//2 * OBJECT_CONSTRAINT_MAX

class Distribution(Enum):
    SMALL_VALUES = 1
    HIGHT_VALUES = 2
    EVEN_VALUES = 3

class Codistribution(Enum):
    SMALL_VALUE_HIGHT_WEIGHT = 1
    SMALL_VALUE_SMALL_WEIGHT = 2
    PROPORTIONAL_VALUE_WEIGHT = 3
    HIGHT_VALUE_HIGHT_WEIGHT = 4
    HIGHT_VALUE_SMALL_WEIGHT = 5



def generate_independent_data_distribution(number, Distribution, param) -> list:
    """
    Create an array of random in generated with the given distribution. The value
    of those int a between 1 and OBJECT_CONSTRAINT_MAX.
    Parameters:
    -----------
        number: int
            The number of values to generate.
        Distribution: enum
            The distribution used for generating the values
        param: float
            The param for influancing the values generated.

    Returns:
    --------
        x: list
            The array of integer randomly generated according to the given dispersion.
    """

    if Distribution == Distribution.SMALL_VALUES:
        x = [min(int(random.paretovariate(0.5)), OBJECT_CONSTRAINT_MAX) for i in range(0, number)]
    elif Distribution == Distribution.HIGHT_VALUES:
        # first parameter: increase this param increase the frequence of hight value and their value
        # second parameter: how values differe from each other, if it is close to 0 or 0 value will be even
        #x = [min(int(random.lognormvariate(4.65, 2.75)), OBJECT_CONSTRAINT_MAX)  for i in range(0, number**2)]
        x = [min(int(random.triangular(1, OBJECT_CONSTRAINT_MAX, OBJECT_CONSTRAINT_MAX)), OBJECT_CONSTRAINT_MAX)  for i in range(0, number)]
    else: # EVEN_VALUES
        #x = [min(int(random.triangular(0, 100, 55)), OBJECT_CONSTRAINT_MAX)  for i in range(0, number)]
        # close to 55, and between 52-57
        x = [min(int(random.lognormvariate(4, 0.02)), OBJECT_CONSTRAINT_MAX)  for i in range(0, number)]

    return x


def generate_codependent_data_distribution(number, Codistribution) -> list:
    """
    Generate 2 arrays of values that are correlated.
    """
    if Codistribution == Codistribution.SMALL_VALUE_HIGHT_WEIGHT:
        x = generate_independent_data_distribution(number, Distribution.SMALL_VALUES, 0)
        y = []
        for i in range(0, number):
            y += min(math.exp(-x[i]), OBJECT_CONSTRAINT_MAX)

    if Codistribution == Codistribution.SMALL_VALUE_SMALL_WEIGHT:
        x = generate_independent_data_distribution(number, Distribution.SMALL_VALUES, 0)
        y = []
        for i in range(0, number):
            y += min(math.exp(x[i]), OBJECT_CONSTRAINT_MAX)

    else: #PROPORTIONAL_VALUE_WEIGHT
        x = generate_independent_data_distribution(number, Distribution.SMALL_VALUES, 0)
        y =x.copy()

    return (x, y)


def get_parse_args() -> Namespace:
    """
        Defines the options availibles on the command line:
        >python3 generator.py W N -v 1 -vp 2.1 -w 1 -wp 2.1

        Mandatory arguments:
        --------------------
            W: int
                The maximum weight of the knapsack
            N: int
                The number of items

        Optionnal arguments:
        --------------------
            --value: int (-v)
                Correspond to the value of the enum Distribution.
                This is the choice of the distribution for values of items.
            --vparameter: float (-vp)
                Parameter to control the wider of the dispertion of values.
            --weight: int (-w)
                Correspond to the value of the enum Distribution.
                This is the choice of the distribution for weights of items.
            --wparameter: float (-wp)
                Parameter to control the wider of the dispertion of weights.

        return: 
        -------
            args :dict
                key: name of a parameter
                value: value of a parameter
    """
    parser = argparse.ArgumentParser(
        prog="Problems Generator",
        description="Generate a 0/1 knapsack problem with the given parameters",
        epilog="Advanced Algorithm Project 2022"
    )
    parser.add_argument('knapsack_problem_file',
        type=str,
        help="The name of the file in which saved the knapsack problem generated."
    )
    parser.add_argument('knapsack_weight', 
        choices=range(1, MAX_KNAPSACK_WEIGHT), # limit values ti a specific set of choices
        type=int, # convert automaticaly the value with the given type, and handle error
        metavar="W", # the name display in the help message
        help="The weight of the knapsack is a positive integer between 1 and " + str(MAX_KNAPSACK_WEIGHT),   
    )
    parser.add_argument('number_of_items',
        choices=range(1, MAX_NUMBER_OF_ITEMS),
        type=int,
        metavar="N",
        help="The number of items of the generated problem is an integer between 1 and " + str(MAX_NUMBER_OF_ITEMS)
    )
    parser.add_argument("--values", "-v",
        choices=range(1, len(Distribution)+1),
        type=int,
        help="Choose a valid distribution function for the values of items."
    )
    parser.add_argument("--vparameter", "-vp",
        type=float,
        help="Parameter to control the distribution range of the value, highter is the param wilder is the distribution"
    )
    parser.add_argument("--weight", "-w",
        choices=range(1, len(Distribution)+1),
        type=int,
        help="Choose a valid distribution function for the weight of items."
    )
    parser.add_argument("--wparameter", "-wp",
        type=float,
        help="Parameter to control the distribution range of the weight, highter is the param wilder is the distribution"
    )

    return parser.parse_args()


# >python3 generator.py file knapsack_weight number_of_items [--values distribution_function -vp parameter -vmin 1 -vmax 50] [--weight distribution_function -wp parameter -wmin 1 -wmax 100]
def main():
    # analyse the command line with a parser
    args = get_parse_args()
    print(args)

    # -----------------#
    # generate problem #
    # -----------------#
    W=args.knapsack_weight
    N=args.number_of_items

    n=len(Distribution)

    # value distribution
    if args.values and args.values <= n:
        distribution_of_values = Distribution(args.values)
    else:
        distribution_of_values = Distribution(random.randint(1, n))
    if args.vparameter:
        value_parameter = args.vparameter
    else:
        value_parameter = 1.5
    print("value distribution:" + str(distribution_of_values) + ";param: "+ str(value_parameter))

    # weight distribution
    if args.weight and args.weight <= n:
        distribution_of_weight = Distribution(args.weight)
    else:
        distribution_of_weight= Distribution(random.randint(1, n))
    if args.wparameter:
        weight_parameter = args.wparameter
    else:
        weight_parameter = 1.5
    print("weight distribution:" + str(distribution_of_weight) + ";param: "+ str(weight_parameter))

    # generate data given the Distribution
    values= generate_independent_data_distribution(N, distribution_of_values, value_parameter)
    weights= generate_independent_data_distribution(N, distribution_of_weight, weight_parameter)

    # print the knapsack problem
    print(str(N)+" "+str(W))
    for i in range(0, N):
        print(str(values[i])+" "+str(weights[i]))

    # save the problem into a file
    with open(args.knapsack_problem_file, 'w') as f:
        s = str(N)+" "+str(W)
        f.write(s+"\n"); print(s) # debbug display

        for i in range(0, N):
            s = str(values[i])+" "+str(weights[i])
            f.write(s+"\n"); print(s) # debbug display

        # write at the end of the file the distribution
        f.write("value distribution:" + str(distribution_of_values) + ";param: "+ str(value_parameter)+"\n")
        f.write("weight distribution:" + str(distribution_of_weight) + ";param: "+ str(weight_parameter))
        
        # confirmation message
        print("knapsack problem saved in problems/"+args.knapsack_problem_file)


if __name__ == "__main__":
    import argparse
    main()