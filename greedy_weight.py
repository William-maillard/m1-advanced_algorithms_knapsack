"""
To launch the greedy approach with the version of the array sorted
by ratio weight.
That allow us to save the results (with a bash script) into a 
sub folder greedy/greedy_weight

Author: William 
"""
from greedy import greedy
from greedy import sort_by_weight

def greedy_weight(max_weight, items):
    return greedy(max_weight, items, sort_by_weight)

if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(greedy_weight)