"""
Ant colony optimization algorithm:

Ressource: this approach was realize thank to the following reearch paper:
'ant algorithm  for the multidimensional knapsack problem' from
'Ines Alaya, Christine Solnon, Khaled Ghedira'
https://hal.archives-ouvertes.fr/hal-01541529

We want to find a less weighted path in a graph, and for that
we simulate its exploration by artificial ants that release pheromones.
Ants tends to take the path with more pheromone, thus we need to consider
realeasing more pheromones on path that have a small weight.

We will consider pheromones between to items of knapsack, meaning this
represennt the probability of an item is chose given the other one is chosen.

----------------------------------------------------------------------------

Construction of the graph:


Author: William 
"""
import numpy as np

MAX_LEVEL_OF_PHEROMONE = 10
MIN_LEVEL_OF_PHEROMONE = 0
NB_ANTS = 20
MAX_NUMBER_OF_EXPLORATIONS = 100

def ant_colony(max_weight, items):
    """
        # TODO: initialize a pheronome trail to their max vaalue
        ant_exploring_the_graph = True
        nb_exploration = 0

        while ant_exploring_the_graph and nb_exploration < MAX_NUMBER_OF_EXPLORATIONS:
            nb_exploration += 1

            # create a solution for each ant
            for ant in range(NB_ANTS):
                # chose a random items depanding on its atractness with pheromones
                # add items to fill the knapsack, higher the pheromones higher the chance to be selected
                pass
            pass

            # update pheromone trail => evaporation + adding pheromon to couple of items selected
            # make sure that the pheromone trail is bounded
    """
    n_items = len(items)
    alpha = 1
    beta = 1
    rho = 0.1
    q = 100

    # Initialize the solution matrix
    solutions = np.zeros((NB_ANTS, n_items))

    # Initialize the pheromone matrix
    pheromones = np.ones((NB_ANTS, n_items)) / n_items

    # Initialize the heuristic matrix
    heuristics = np.array([[item[1] / item[0] for item in items] for _ in range(NB_ANTS)])

    # Initialize the total weight and value of each solution
    total_weight = np.zeros(NB_ANTS)
    total_value = np.zeros(NB_ANTS)

    # Iterate for the specified number of iterations
    for i in range(MAX_NUMBER_OF_EXPLORATIONS):
        # For each ant, calculate the probability of selecting each item
        for j in range(NB_ANTS):
            probabilities = np.zeros(n_items)
        for k in range(n_items):
            if total_weight[j] + items[k][0] <= max_weight:
                probabilities[k] = (pheromones[j][k] ** alpha) * (heuristics[j][k] ** beta)
        probabilities /= probabilities.sum()

        # Select the items for the ant using the probabilities
        for k in range(n_items):
            if total_weight[j] + items[k][0] <= max_weight:
             solutions[j][k] = np.random.random() < probabilities[k]
            if solutions[j][k]:
                total_weight[j] += items[k][0]
                total_value[j] += items[k][1]

        # Update the pheromone matrix
        for j in range(NB_ANTS):
            for k in range(n_items):
                pheromones[j][k] *= (1 - rho)
                if solutions[j][k]:
                    pheromones[j][k] += rho / total_value[j]

    # Return the best solution
    return solutions


if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(ant_colony)