"""
This module implements the dynamic programming approach.

The sub-problem structure consider in this module:
We consider a sub-problem with a limit weight w < max knapsack weight and 
n cases (n is the number of item) where we consider {0,.., i} items (i<n), 
with i increasing at each case.

----------------------------------------------------------------------------

So, according to the Solution of execice sheet 3, we have 2 cases:
let OPT(i, w): 
    maximum gain (value) from the subset of items 1, ..., i with limit w

First case:
    item i is not in the optimal solution, thus we consider a solution with
    {0, ..., i-1} items (previously calculated but with a greater weight).
    OPT(i, w) = OPT(i-1, w)

Second case:
    item i can be select in the optimal solution, thus we build a solution
    with it and look for a solution with {0, ..., i-1} with a new weight:
    w - weight item i.
    Then we need to compare with previous solutions to find if it is worthy 
    to take item i.

----------------------------------------------------------------------------

We obtain this formula: (from the correction of exercice sheet 3)
OPT(i, w) =
    | 0                                           if i == 0
    | 0                                           if w == 0
    | OPT(i-1, w)                                 if wi > w (i.e we can't select item i)
    | max({OPT(i-1, w), vi + OPT(i-1, w-wi)})     otherwise (general case)

where vi (resp. wi) is value (resp. weight) of item i

----------------------------------------------------------------------------

Implementation:
We store each subproblem solution into an array size w*n as describe below:
(w= max wieght of the knapsack, n= number of items)
    - row i means we consider the items {0, ..., i}
    - column i means we consider a max weight of i
Then to find the solution we consider the cell w*n wich is the optimal value
of the knapsack.
To retrieve the item selected in the solution, thus the weight of the solution,
we proceed as below:
let vij = the cell at column i and row j
    - if value of obect j <= v, we take this object into the solution, and we
      look the above row to select the cell with v(i-1, k)
    - else we go up from 1 row
We repeat the process until we reach the top of the array.

----------------------------------------------------------------------------

Problem with the first approach: 
                                    => multiple lines are identical
Indeed, we don't need to consider all weight in {0, ..., max_knapsack_weight}.
We need to compute only the value that will be used for building the solution.
So instead of strating with the 1 cell ([1][1]), we will stard at the last one
([max_weight][n]) an recursively compute each values we need.
The algorithm also store each value it computes in an array, thus it will
compute the needed values only one.

Author: William 
"""
VALUE_INDEX = 0
WEIGHT_INDEX = 1

def dynamic(max_weight, items):
    """
    """
    # initialization, the array that store the optimal value foreach cases
    nb_items = items[0]
    opt = [[0 for i in range(nb_items+1)] for j in range(max_weight+1)]
    solution = [0 for i in range(nb_items+1)]


    # NB we start at 1 because we already initialize the array at 0
    # for each row, i.e we consider items from {i, ..., item_max}
    for item_max in range(1, nb_items+1):
        
        # the columns
        for weight in range(1, max_weight+1):
            
            # does the item_max fit in the knapsack ?
            if(items[item_max][WEIGHT_INDEX] <= weight):
                # We take the best value of the 2 cases above => it is worth to take the item ?
                # case 1: we select the item, and add the value of the previous row for weight - (weight of item_max)
                # case 2: we not select the item, thus we take the value of the previous row
                new_weight = weight-items[item_max][WEIGHT_INDEX]
                opt[weight][item_max] = max(
                    items[item_max][VALUE_INDEX] + opt[new_weight][item_max-1], # case 1
                    opt[weight][item_max -1] # case 2
                )
            else: 
                # item_max can't be chosen, we look for the solution for {0,..., item_max-1} with same weight
                # This correspond to the cell above, compute at the past iteration.
                opt[weight][item_max] = opt[weight][item_max -1]

    # Now we need to retrieve the solution from the array
    knapsack_value = opt[weight][item_max]
    knapsack_weight = 0
    current_item = item_max
    availible_weight = weight

    # we browse the rows from the bottom row to the top
    # the colomn from the right (max weight) to the left (no more space in the knapsack)
    while current_item > 0:
        # can we take the curent item in the knapsack ?
        if items[current_item][WEIGHT_INDEX] <= availible_weight:

            # can we take an item with less weight for the same total value ?
            while opt[availible_weight][current_item] == opt[availible_weight][current_item-1]:
                current_item -= 1
            
            solution[current_item] += 1
            availible_weight -= items[current_item][WEIGHT_INDEX]
            pass

        # next item
        current_item -= 1

    # The final weight of the knapsack
    knapsack_weight = max_weight - availible_weight
    solution[0] = (knapsack_value, knapsack_weight)
    
    return solution

if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(dynamic)