"""
This module implement the greedy algorithm to solve the 0/1 knapsack problem.
It uses 3 differents ways to sort the list of items:
- value
- weight
- ratio value/weight

Author: William 
"""
from utilities.solution_converter import format_solution_into_array
VALUE_INDEX = 0
WEIGHT_INDEX = 1

# Functions to sort the items :
# NB: according to the python documentation the sorted function sort a list
# of n items, with a complexity of o(nlogn)
def sort_by_value_weight(items) -> list:
    """
    Sort the given list of items by decreasing ratio value/weight, and
    if equal, by increasing weight.
    """
    return sorted(items, key=lambda t: (t[VALUE_INDEX]/t[WEIGHT_INDEX], -t[WEIGHT_INDEX]), reverse=True)

def sort_by_value(items) -> list:
    """
    Sort the given list of items by decreasing value, and
    if equal, by increasing weight
    """
    return sorted(items, key= lambda t: (t[VALUE_INDEX], -t[WEIGHT_INDEX]), reverse=True)

def sort_by_weight(items) -> list:
    """
    Sort the given list of items by increasing weight, and
    if equal, by decreasing value
    """
    return sorted(items, key= lambda t: (t[WEIGHT_INDEX], -t[VALUE_INDEX]) , reverse=False)

# -----------------------------

def greedy(max_weight, items, sort_function):
    """
    First, the algorithm sort the list of item using the python function sorted
    that sort a list (given a function) in o(nlogn)
    Then the greedy algorithm pick each item it can fit into the knapsack, begining
    at the index 0 of the list.

    Parameters:
        -----------
        max_weight: int
            The maximum weight the knapsack can hold.

        items: list
            [n, (v1, w1), (v2, w2), ..., (vn, wn)] with:
                - n the number of items
                - vi the value of the i° item
                - wi the weight of the i° item
        
        sort_function: function
            The function used to sort the list of items.

        Returns:
        --------
        solution: tuple(Value, weight)
            The value and weight of the solution of the problem.
    """
    n = items.pop(0)
    knapsack = [] # list containing the items chosen
    solution = [0, 0] # value and weight
    sorted_items = sort_function(items)
    # reinit the state of the list as the beginning
    items.insert(0, n)
    
    # loop variables
    i, knapsack_full, n = 0, False, len(sorted_items)# NB len is in o(1)
    
    # we take the first item of the list until we can't fit anymore items
    while not(knapsack_full) and i<n: 

        # can we add the entire item in the knapsack ?
        if solution[WEIGHT_INDEX] + sorted_items[i][WEIGHT_INDEX] <= max_weight:
            solution[WEIGHT_INDEX] += sorted_items[i][WEIGHT_INDEX]
            solution[VALUE_INDEX] += sorted_items[i][VALUE_INDEX]
            knapsack.append(sorted_items[i])

            # We add the last element to the knapsack
            if solution[WEIGHT_INDEX] + sorted_items[i][WEIGHT_INDEX] == max_weight:
                knapsack_full = True
        
        # Next item
        i += 1

    print(knapsack)

    # Convert the solution according to the unmodified list of items given at the beginnig.
    answer = [(solution[VALUE_INDEX], solution[WEIGHT_INDEX])] + format_solution_into_array(knapsack, items)

    return answer

# To test the execution of the algorithm with all 3 cases
def greedy_value(max_weight, items):
    return greedy(max_weight, items, sort_by_value)

def greedy_weight(max_weight, items):
    return greedy(max_weight, items, sort_by_weight)

def greedy_ratio_value_weight(max_weight, items):
    return greedy(max_weight, items, sort_by_value_weight)


if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    print("Greedy approach\n----------")
    print("Items sort by value")
    solve_knapsack_problem_with_approach(greedy_value)
    print("Items sort by weight")
    solve_knapsack_problem_with_approach(greedy_weight)
    print("Items sort by the ratio value/weight")
    solve_knapsack_problem_with_approach(greedy_ratio_value_weight)
