"""
Program for 0/1 Knapsack problem using Dynamic Programming
Dynamic Programming divides the problems into sub-problems.

Returns the maximum value that can be put in a knapsack of capacity C

Author: Annsana
"""

#Bottom-up approach to solve knapsack problem using Dynamic Programming
def dynamic_programming(v,wt,c):

  """
  Input - set of items, set of weights wt, profit of the items v, and knapsack capacity c
  Output - table which holds the solution of the problem.
  
  """
  #n :number of items
  n = len(v)

  # Build table K[][] in bottom up manner
  k = [[0 for x in range(c + 1)] for x in range(n + 1)]

  for i in range(n+1):        
    # i first items allowed
    for j in range(c+1):      
      # j max wight allowed
      if i==0 or j==0:
        k[i][j] = 0
      elif wt[i-1] <= j: 
        k[i][j] = max(v[i-1] + k[i-1][j-wt[i-1]], k[i-1][j])
      else:
        k[i][j] = k[i-1][j]
  
  return k[n][c]




#Top-down approach to solve knapsack problem using Dynamic Programming

def td_knapSack(c, wt, v, n):
    '''
    This approach follows memoization method 
    also known as top-down approach
    Here we solve the subproblems which are needed

    '''
    #Base case
    if n == 0 or c == 0:
        return 0
    if wt[n-1] <= c: 
        #check if the weight is not exceeding the capacity . 
        #Return the maximum of two cases where first one is including the nth item and second is not includinh the nth item.
        
        return (max(v[n-1]+td_knapSack(c-wt[n-1], wt, v, n-1), td_knapSack(c, wt, v, n-1))) 
        #this follows recursive method
    
    #if weight of nth item is more than knapsack capacity c then this item cannot be included in the optimal solution.
    else:
        return (td_knapSack(c, wt, v, n-1))







#calling all the functions
v = [50, 100, 150, 200]
wt = [8, 16, 32, 40]
c = 64

print("The solution for knapsack using bottom-up approach is :"+ str(dynamic_programming(v, wt, c)))
print("The solution for knapsack using top-down approach is :" + str(td_knapSack(c, wt, v,len(v))))