""""
For more details explanations, see doc module of dynamic.py

Author: William 
"""
VALUE_INDEX = 0
WEIGHT_INDEX = 1


# !!!!!!! We need to increase the recursion limit fix by python !!!!!!!!!!!!
# by default its 10^-3 but can variate depending on your OS/computer
# It's a safety for infinit loops.
import sys
from generator import MAX_NUMBER_OF_ITEMS
sys.setrecursionlimit(MAX_NUMBER_OF_ITEMS**2)
# we do recursion on rows => items
# thus we can have the stack fill with MAX_NUMBER_OF_ITEMS for our personal pbm
# The exponent 2 is to be safe with problem from external data set.
# --------------------------------------------------------------------------


def dynamic_top_down(max_weight, items):
    """
    """
    # initialization, the array that store the optimal value foreach cases
    # -1 indicate that the value as not been calculated yet.
    nb_items = items[0]
    opt = [[-1 for i in range(nb_items+1)] for j in range(max_weight+1)]
    solution = [0 for i in range(nb_items+1)]

    # We start at the bootom-right cell, an recursively compute the value
    # until the top is reach
    fill_opt_array_rec(opt, items, max_weight, nb_items)

    # Now we need to retrieve the solution from the array
    knapsack_value = opt[max_weight][nb_items]
    knapsack_weight = 0
    current_item = nb_items
    availible_weight = max_weight

    # we browse the rows from the bottom row to the top
    # the colomn from the right (max weight) to the left (no more space in the knapsack)
    while current_item > 0:
        # can we take the curent item in the knapsack ?
        if items[current_item][WEIGHT_INDEX] <= availible_weight:

            # can we take an item with less weight for the same total value ?
            while opt[availible_weight][current_item] == opt[availible_weight][current_item-1]:
                current_item -= 1
            
            solution[current_item] += 1
            availible_weight -= items[current_item][WEIGHT_INDEX]
            pass

        # next item
        current_item -= 1

    # The final weight of the knapsack
    knapsack_weight = max_weight - availible_weight
    solution[0] = (knapsack_value, knapsack_weight)
    
    return solution

def fill_opt_array_rec(opt, items, weight, item):
    """
    """
    # Stop case, we reach the top of the array
    if weight == 0 or item == 0:
        opt[weight][item] = 0
        return

    previous_item = item-1

    # If the above cell is not compute, we do it
    if opt[weight][previous_item] == -1:
        fill_opt_array_rec(opt, items, weight, previous_item)

    # Can we fit the current item in the knapsack ?
    if items[item][WEIGHT_INDEX] > weight:
        # no, so this is the same solution as above
        opt[weight][item] = opt[weight][previous_item]

    else:
        # we take the max of solution with item and the one whitout the item

        # First we look if we already compute the solution with a weight
        # less the weight of the selected current item.
        weight_without_item = weight-items[item][WEIGHT_INDEX]
        if opt[weight_without_item][previous_item] == -1:
            # No, thus we compute it
            fill_opt_array_rec(opt, items, weight_without_item, previous_item)

        opt[weight][item] = max(
            opt[weight][previous_item],
            opt[weight_without_item][previous_item] + items[item][VALUE_INDEX]
        )


if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(dynamic_top_down)