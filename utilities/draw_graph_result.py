""""
This module draw graph given a directory where we can find
result folder for each method.

Author: William 
"""
import os
import pylab as py

graphic_dir = "./graphics/"

def draw_graph_external_data_set(result_directory, optimum_directory):
    """
    Draw and save graph of the results obtain with each approach, that can
    be find in result_directory.
    optimum_directory contains the optimal solution of the pbm to compare
    with the one found by our algorithm.
    """

    # data come from an external data set
    if optimum_directory != "":
        pbm_dir="./external_data_set/"
        result_dir = "./results/external_data_set/"
        # to create a dir with the same name in ./graphics
        problem_dir = "./external_data_set/"+result_directory # to find the associated problem file    
        # get the folder where optimal solutions can be found
        opt_dir = pbm_dir+optimum_directory
    else:
        problem_dir = "./problems/"
        result_dir = "./results/problems/"

    # get the subfolders for each approach results
    dir_approaches = os.listdir(result_dir+result_directory)

    # draw a graphics for each approach
    approach_coordinates = [[], []]
    approaches_values  = []

    for approach in dir_approaches:
        # the file to save the graphic
        #file_png = graphic_dir+result_directory+"/"+approach+".png"
        file_pdf = graphic_dir+result_directory+"/"+approach+".pdf"
        file_values_pdf = graphic_dir+result_directory+"/"+approach+"_values.pdf"

        # x number of items and y the execution time of the algorithm
        x1, y1, y2 = [], [], []
        # value of the optimal solution
        v_optimal, values = [], []

        # load results in an array :
        for result_file in os.listdir(result_dir+result_directory+"/"+approach):

            # store infos of the result file
            time_execution = value = weight =  0
            solution = []


            # open the result file and load informations it contains
            with open(result_dir+result_directory+"/"+approach+"/"+result_file, 'r') as f:
                
                time_execution = int(f.readline())

                value, weight = f.readline().split(" ")
                value, weight = int(value), int(weight)

                values.append(value)

                solution = f.readline().split(" ")
                pass
            
            if optimum_directory != "":
                # get the optimal solution
                with open(opt_dir+"/"+result_file) as f:
                    v_optimal.append(int(f.readline()))
                    print("optimal", v_optimal, sep=": ")


            nb_items = len(solution)
            # add to the array
            x1.append(nb_items)
            y1.append(time_execution)
            y2.append(value)
            #print("####RESULTS from file:")
            #print("time execution", time_execution, sep=": ")
            #print("value", value, sep=": ")
            #print("weight", weight, sep=": ")
            #print("solution", solution, sep=": ")
            #print("Number items ", nb_items)



        # draw the graphic for this approach time in function of nb items
        title = "Knapsack resolution with "+approach+"\n"+result_directory
        graph = draw_graph(x1, y1, title, "number of items", "Time of execution in ns")
        graph.show()
        approach_coordinates[0].append(x1)
        approach_coordinates[1].append(y1)
        approaches_values.append(y2)
        save(graph, file_pdf)

        if optimum_directory != "":
            file_pdf_opt = graphic_dir+result_directory+"/"+approach+"_optimal_cmp.pdf"
            # draw the graphic value find / ratio value_find/optimal_value
            graph_opt = draw_graph(values, [values[i]/v_optimal[i] for i in range(len(values))],
                 title, "value found", "ratio (value found)/(optimal value)")
            save(graph_opt, file_pdf_opt)
            graph_opt.show()

        else: # this is a generated problem we need to draw the value found by the number of items
            title = "Knapsack resolution with "+approach+"\n"+result_directory
            graph = draw_graph(x1, values, title, "number of items", "Solution value find")
            graph.show()
            approach_coordinates[0].append(x1)
            approach_coordinates[1].append(values)
            save(graph, file_values_pdf)

    
    graph = draw_graph_all_approaches(approach_coordinates, dir_approaches, "Knapsack resolution of\n"+result_directory, "number of items", "Time of execution in ns")
    save(graph, graphic_dir+result_directory+"/all.pdf")

    # if its is a problem from the generator, draw value found depending of number of items.
    if optimum_directory == "":
        approach_coordinates[1] = approaches_values  
        print("######################")
        print("x: ", len(approach_coordinates[0]), "  y: ", len(approach_coordinates[1]))

        graph = draw_graph_all_approaches(approach_coordinates, dir_approaches, "Knapsack resolution of\n"+result_directory, "number of items", "value find")
        save(graph, graphic_dir+result_directory+"/all_values.pdf")


def save(graph, file_name):
    graph.savefig(file_name)


def draw_graph(x, y, title, xlabel, ylabel):
    """
        Draw a graph with the given informations.
        Parameters:
        -----------
        x: list
            The abssysces of points to draw.
        y: list
            The ordinate of points to draw (len(y) must be equal to len(x)).
        title: str
            The title of the displayed graph.
        xlabel: str
            The abssyces label of the displaying graph.
        ylabel: str
            The ordinates label of the displaying graph.
        
        Returns:
        --------
        graph: object
            The figure object to represent the graph.
    """
    graph = py.figure()
    py.plot(x, y, "r+")
    py.grid()
    py.xlabel(xlabel)
    py.ylabel(ylabel)
    py.title(title)
    #py.show()
    return graph

def draw_graph_all_approaches(coord, approach_name, title, xlabel, ylabel):
    """
    Draw the graph of all approavhes.

    Parameters:
    -----------
    coord: list
        It's an array of list, such that coord[i] contains the coordinates
        array x and y of an approaches.
    approach_name: str
        Contains space separated approches names.
    title: str
        The title of the graph to create.
        
    Returns:
    --------
    graph: object
        The figure object to represent the graph.
    """
    i = 0
    graph = py.figure()

    for approach in approach_name:
        x = coord[0][i]
        y = coord[1][i]

        # remove approaches that have not been run on large data set, for exemple brute force
        if len(x) != 0:     
            py.scatter(x, y, marker="+", label=approach)
        
        i += 1


    py.grid()
    py.xlabel(xlabel)
    py.ylabel(ylabel)
    py.title(title)

    py.legend(loc='best')
    #py.show()
    
    return graph



if __name__ == "__main__":
    import argparse
    from os.path import isdir
    from os import makedirs

    parser = argparse.ArgumentParser(
        prog="knapsack problem draw graph result",
        description="Draw result graphic for the results obtains with our algorithms.",
        epilog="Advanced Algorithm Project 2022"
    )
    parser.add_argument('result_dir',
        type=str,
        help="The directory of ./results/external_data_set in which to find files result."
    )
    parser.add_argument('-opt',
        type=str,
        help="The directory where to find optimal solutions.",
        default=""
    )
    args = parser.parse_args()

    d= graphic_dir+args.result_dir
    if not isdir(d):
        makedirs(d)
        pass

    #draw_graph_external_data_set("./results/external_data_set/set_1/large_scale")
    draw_graph_external_data_set(args.result_dir, args.opt)