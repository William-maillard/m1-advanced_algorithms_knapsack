# Author: William 
import argparse
import time
from utilities.knapsack_problem_loader import load_knapsack_problem_from_file
from utilities.save_results import save_results
from main import print_time

def solve_knapsack_problem_with_approach(approach):
    """
        Used for tested each algorithm for solving the knapsack problem.
        First it get the path of the file pass as an argument of the program.
        (program in which this function is called)
        Then load the knapsack problem from a file.
        Finally, execute the approach function given as parameter and mesure
        its execution time.

        Parameters:
        -----------
            approach: function
                The function of the algorithm approacvh to solve the knapsack problem.                
    """  

    parser = argparse.ArgumentParser(
        prog="knapsack problem solver",
        description="Solve the knapsack problem, find in the given file, with the given approach.",
        epilog="Advanced Algorithm Project 2022"
    )
    parser.add_argument('knapsack_problem_file',
        type=str,
        help="The name of the file in which the knapsack problem is."
    )
    parser.add_argument("--file", "-f",
    type=str,
    help="Chose a file in which to save the results of the algorithm.\n(time, total value and weight, items selected)"

    )
    args = parser.parse_args()

    # extrat the problem from a file
    W, items = load_knapsack_problem_from_file(args.knapsack_problem_file)

    print("Knapsack problem: ")
    print(str(W)+" "+str(items))

    start = time.perf_counter_ns()
    solution = approach(W, items)
    t = time.perf_counter_ns() - start
    

    print("solution: "+str(solution))
    print_time(t)

    # save the results:
    if args.file:
        save_results(args.file, t, solution[0][0], solution[0][1], solution[1:])


if __name__ == "__main__":
    print("Error, this module is not suppose to be used by the command line.")