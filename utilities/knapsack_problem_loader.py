"""
This module contains functions to load a knapsack problem
from a file.

Author: William 
"""
import utilities.save_results

def load_knapsack_problem_from_file(path_file) -> tuple:
    """
    Load the items and maximum knapsack weight from a file containing a knapsack
    problem with the following format: 
    File format:
    -------------
    N W
    V1 W1
    V2 W2
    ... ...
    VN WN

    Parameters:
        -----------
        path_file: String
            The relative path of a valid file containing the knapsack pronblem.

    Returns:
    --------
        (w, items):
            w is the maximun weight of the knapsack
            items is the list of tuple (value, weight) of items

    Errors :
    --------
        return (0, [])
    """
    W = 0
    items = []
    with open(path_file, 'r') as f:
        # take max knapsack weight and number of items
        N, W = f.readline().split(" ")
        N, W = int(N), int(W)

        print("N, W = ", end="")
        print((N, W))

        items = [N]
        for i in range(0, N):
            v, w = f.readline().split(" ")
            v, w = int(v), int(w)
            items.append((v, w))
    
    return (W, items)