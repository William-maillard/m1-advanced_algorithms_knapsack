"""
Result file:
time_execution_in_nanoseconds
total_weight total_value
[0, 1, 0, ...., 0, 1] => array representing the item selected

Author: William 
"""

def save_results(path, time, total_value, total_weight, solution_items):
    """
        Save the given result in the given file.
        Parameters:
        -----------
        path: str
            The path of the file in which to write results. It overwrite
            existing files.

        time: float
            Result time of execution of the algorithm.

        total_value: int
            The value of the knapsack solution found.

        total_weight: int
            The weight of the knapsack solution found.
        
        solution_items: list
            A List of 0 and 1 to indicate which items of the knapsack problem
            have been selected in the solution.
    """
    with open(path, 'w') as result_file:
        result_file.write(str(time))
        result_file.write("\n")
        result_file.write(str(total_value)+" "+str(total_weight))
        result_file.write("\n")
        result_file.write(" ".join(map(str, solution_items)))


if __name__ == "__main__":
    print("This file contains only a function to save the results of our knapsack problem resolvers.")