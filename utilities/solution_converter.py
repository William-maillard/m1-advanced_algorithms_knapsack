"""
Some algorithm modify the base list, thus we need this
module to retrive the solution as a binary (0 and 1) array
which indicate that item i from the original list of items
is in the knapsack if array[i]=1.

Author: William 
"""
def format_solution_into_array(items_in_solution, original_list_of_items):
    """
        Parameters:
        -----------
        item_in_solution: list
            items in format (value, weight) that are selected in
            the solution

        original_list_of_items: list
            the original list of items of the knapsack problem
            (before its modification with the algorithm)


        Returns:
        --------
        knapsack_solution: list
            list of 0 and 1 that indicate if the item i is present
            or no in the solution
    """
    knapsack_solution = [0 for i in range(original_list_of_items[0]+1)]
    
    # its in o(n^2) but it's necessary to get the solution in this format
    # for comparing the one found with the one in the data set.
    for item in items_in_solution:
        knapsack_solution[original_list_of_items.index(item)] = 1

    knapsack_solution.pop(0) # at index 0 we store (value, weight)

    return knapsack_solution