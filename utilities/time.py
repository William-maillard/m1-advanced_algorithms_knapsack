#Author: William 
from datetime import timedelta

def print_time(t):
    """
        Print the time in the following format:
        HH:MM:SS

        Parameters:
        -----------
            t: float
                time in nanaseconds
    """
    t_in_ms = t * 10**-3
    print("time (HH:MM:SS) ", end="")
    print(timedelta(microseconds=t_in_ms, ))