"""
The idea for the algorithm is to do a width exploration of a tree.
The root its the solution containing all the items we can fit in a fractional knapsack.
Then we subdivise each node in two branch considering an item xi:
    - we keep the item xi
    - we discard the item xi
At each depth step we kill branches that can't contains an optimal solution, 
that is to say a branch that contain a value (cost) < to the optimal one (global upper bound).
Each node contains two values (corresponding to the sum of items value):
    - upper bound (whithout fraction)
    - cost (with fraction of items to fill the knapsack)
NB:
We transform a maximisation problem into a minimalisation one, thus
we will consider negative cost an upper bound.
For the construction of the root node, we chose to take the solution of the
fractionnal knapsack problem by a greedy algorithm (item sort by value/weight)
It's based on the fact that the greedy algo chose the optimal solution
for the fractionnal knapsack problem, thus we will start with an upperbound
closer to the optimal and reduce the branch exploration.
1) Sort the items by value/weight ratio
2) compute the solution of the fractional knapsack problem as the begining heuristic
3) The root of the tree is the heuristic, then we cosider adding/deleting
one item each time we subtivise a node
4) Each time we change the upper bound we look if we need to kill some branch
(i.e if the solution on that branch is less than the best solution find so far.)
5) At each begining of the loop we take the node with the least cost.
sources to understand how to used this method:
    -https://en.wikipedia.org/wiki/Branch_and_bound
    -https://www.youtube.com/watch?v=yV1d-b_NeK8&list=PLAPWd_E2WK7Otz61pdpe0GC2urkNSrxJE&index=5&t=9s


Author: William 
"""
from utilities.solution_converter import format_solution_into_array
VALUE_INDEX = 0
WEIGHT_INDEX = 1



class Node:
    """
    A node store the upper bound (solution of the branch) and a cost (solution of fractional knapsack)
    If the cost is greater than the les upper bound found so far (best solution) then there is no need
    to explore further the branch.
    If the upper bound of the node is greater than the current one, we update it and check the validity
    of each node to explore.
    It also store the total weight and total value that correspond to the solution of its branch.
    It is associated with an item (corresponding to its depth) and a boolean to know if this item is 
    selected for the solution or not.
    As the optimal solution found is the last node remaining, we need to remember for each node its parent.
    Thus we store this information into a node.
    """

    # nb items in the root node, in order to do less calculation
    nb_items_in_heuristic = 0
    # the list of items of the knaposack problem
    items = []
    # the maximum knapsack weight
    knapsack_weight = 0


    def __init__(self, upper_bound, cost, total_weight, total_value, item_index, item_selected, parent_node) -> None:
        self.upper_bound = upper_bound
        self.cost = cost
        # store the weight and value of the current solution
        # i.e correspond to the sum of all weights/values of the items
        # included in the branch from the root to this node.
        self.total_weight = total_weight
        self.total_value = total_value

        # Item selected by the current node
        # NB: correspond to the depth of the node in the tree
        self.item_index = item_index
        # Boolean to know if we select/remove the item from the knapsack
        self.item_selected = item_selected
        # link to the parent, to retrieve the solution at the end
        self.parent_node = parent_node

    @classmethod
    def create_child_from_a_parent_node(cls, parent_node, item_selected) -> object:
        return Node(parent_node.upper_bound, parent_node.cost, 
            parent_node.total_weight, parent_node.total_value,
            parent_node.item_index + 1, item_selected, parent_node)

    @classmethod
    def isInferior(cls, node_1, node_2) -> bool:
        """
        Used to compare two node from their cost.
        That allow us to sort the list of nodes to explore by the lowest cost.
        return true if node_1 as a less cost than node_2
        """
        return node_1.cost < node_2.cost

    def split(self) -> tuple:
        """
        Subdivise a node into 2, with one that include the next item and 1 that exclude it.
        The next item is representing by item_index+1
        """
        # create 2 child and configure them
        child_node_1 = Node.create_child_from_a_parent_node(self, True)
        child_node_2 = Node.create_child_from_a_parent_node(self, False)

        # special case when we consider items that are include in the heuristic
        if child_node_1.item_index < Node.nb_items_in_heuristic:
            # We keep the item for node 1 thus nothing change
            # We remove the item for node 2 thus we update its value and weight
            child_node_2.total_weight -= Node.items[child_node_2.item_index][WEIGHT_INDEX]
            child_node_2.total_value -= Node.items[child_node_2.item_index][VALUE_INDEX]

            #  remove the value of the item of the upper bound and cost
            child_node_2.upper_bound += Node.items[child_node_2.item_index][VALUE_INDEX]
            child_node_2.cost = child_node_2.upper_bound # we also remove fraction of item add to the cost

            # now we update cost and upperbound
            # TODO: make a function to check if we need to update the upperbound or kill the node
            tmps = child_node_2.item_index
            child_node_2.item_index = Node.nb_items_in_heuristic # to add item that are not currently in the solution
            child_node_2.update_upper_bound_and_cost()
            child_node_2.item_index = tmps # to replace the right index item value
        
        else:
            # we add 1 item to child 1 and nothing to child 2 that not change
            # now we update cost and upperbound
            add_items = child_node_1.update_upper_bound_and_cost()
            if add_items == -1:
                # we can not fit the item at the node index in the knapsack, thus we kill the node
                child_node_1 = None

        # return the two nodes, for updating the global upper bound if needed and
        # add them to the list or kill them if their cost > global upper_bound
        return (child_node_1, child_node_2)

    
    def update_upper_bound_and_cost(self) -> int:
        """
        We iterate other the items list by begining at the self.item_index+1 node.
        We add as many item we can fit and update upperbound and cost of the current
        node.
        Return the index of item we stop.
        """
        knapsack_full = False
        i = self.item_index
        n = len(Node.items) # en o(1)
        
        while i<n and not(knapsack_full) :

            # can we add the entire item in the knapsack ?
            if (self.total_weight + Node.items[i][WEIGHT_INDEX]) <= Node.knapsack_weight :
                # then we add it.
                self.total_weight += Node.items[i][WEIGHT_INDEX]
                self.total_value += Node.items[i][VALUE_INDEX]

                # update upper bound and cost
                # reminder the upper bound is a negative value
                self.upper_bound -= Node.items[i][VALUE_INDEX]
                self.cost -= Node.items[i][VALUE_INDEX]

                # next item
                i += 1
            else:
                # We can't fit the entire item, thus we insert a fraction of it
                # we add value proportionnal of the weight left in the knapsack
                self.cost -= (Node.knapsack_weight - self.total_weight) * (Node.items[i][VALUE_INDEX] / Node.items[i][WEIGHT_INDEX])
                # end of the loop
                knapsack_full = True

        # if we didn't add any item to the knapsack we return -1
        if i == self.item_index:
            return -1

        return i-1

    def is_a_leaf(self):
        """
        If a branch as consider all items (i.e include/exclude each one of them), then
        we reach a solution.
        """
        return self.item_index == len(Node.items)-1 # NB list index begin at 0

    def __str__(self):
        return f'Node(upper_bound: {self.upper_bound}, cost: {self.cost}, total_weight: {self.total_weight}, total_value: {self.total_value}, item_index: {self.item_index}, item_selected: {self.item_selected})'


## --- END of Node class -----



def node_insert_sort_by_cost(list_nodes, node) -> None:
    """
    Insert a node in the global list list_node_to_explore in a way that
    keep the list sorted by increasing cost. (NB: cost is negative)
     Parameters:
        -----------
        list_nodes: List
            A sorted list of node, by increasing order of their cost.
        node: Node
            The node to insert into the list, by keeping it oredered
    Returns:
        --------
        list_nodes: list
            The list_nodes with the node inserted.
    """
    i = 0
    n = len(list_nodes) # o(1)
    greater_cost = True
    while greater_cost and i < n :
        if Node.isInferior(node, list_nodes[i]):
            greater_cost = False
        else:
            i += 1
    
    list_nodes.insert(i, node)
    return list_nodes


def update_node_validity(list_nodes, upper_bound):
    """
    Check if the cost of each node of the list list_nodes_to_explore
    are still inferrior to the upper_bound.
        Parameters:
        -----------
        list_nodes: List
            A sorted list of nodes by their increasing cost.
        upper_bound: int
            The new upper_bound of the tree.
    
    Returns:
    --------
        list_nodes: list
            The updating list node,, i.e whithout the nodes with cost > upper_bound
        
    """
    i = 0
    while i< len(list_nodes) and list_nodes[i].cost <= upper_bound:
        i+=1

    if i != len(list_nodes) : # remove killed nodes
        list_nodes = list_nodes[0:i]

    return list_nodes




# the 'main' function of this module
def branch_and_bound(max_weight, param_items) -> list:
    """ Compute the solution of the knapsack problem with a branch
        and bound approach.
        Parameters:
        -----------
        max_weight: int
            The maximum weight the knapsack can hold.
        items: list
            [n, (v1, w1), (v2, w2), ..., (vn, wn)] with:
                - n the number of items
                - vi the value of the i° item
                - wi the weight of the i° item
        Returns:
        --------
        knapsack: list
            [(V, W), i1, i2, ..., iN] with:
                - V the knapsack's value
                - W the knapsack's weight
                - ij = 1 if the item is in the knapsack, 0 otherwise
    """
    # -- First we sort the list by ratio value/weight --
    n = param_items.pop(0)
    Node.knapsack_weight = max_weight
    #  sort the list by decreasing ratio value/weight and if equal increasing weight
    Node.items = sorted(param_items, key=lambda t: (t[VALUE_INDEX]/t[WEIGHT_INDEX], t[WEIGHT_INDEX]), reverse=True) # nlog(n) python algorithm

    # reinit the list how we find it
    param_items.insert(0, n)

    # Second, create the root node with the heuristic and put it into the list of nodes
    # NB, the heuristic is to take the solution of greedy algorithm of the fractionnal
    # knapsack problem, which is the optimal solution for this type of problem.
    current_node = Node(0, 0, 0, 0, 0, False, None)
    Node.nb_items_in_heuristic = current_node.update_upper_bound_and_cost() + 1 # return an index (that start at 0), thus we add 1 to have the nb of items
    current_node.item_index = -1 # the root node have no item selected, we put 0 before just to calculate upper bound and cost with the function
    

    # Third, we initialize our variables
    list_nodes_to_explore = []
    list_nodes_to_explore.insert(0, current_node)

    global_upper_bound = current_node.upper_bound
    update_upper_bound = False

    list_nodes_solution = []


    # Then we explore the tree until there is no more nodes to explore
    while list_nodes_to_explore:
        # We take the node with the lower cost
        current_node = list_nodes_to_explore.pop(0)

        # We create the two childs of the node
        node_1, node_2 = current_node.split()

        # First we look if we need to update the global upper bound (i.e with find a best solution than the current one)
        if node_1 != None and node_1.upper_bound < global_upper_bound :
            update_upper_bound = True
            global_upper_bound = node_1.upper_bound

        if node_2.upper_bound < global_upper_bound :
            update_upper_bound = True
            global_upper_bound = node_2.upper_bound


        # Then we look if the 2 nodes are in a valid branch, that mean if their cost (fractionnal solution)
        # is lower then the upper_bound (best solution find so far)
        # Thus if their are valid solution to consider, we add them to the list of nodes to explore.
        if node_1 != None and node_1.cost <= global_upper_bound:
            if node_1.is_a_leaf():
                # stop branch exploration case, we find a solution
                list_nodes_solution = node_insert_sort_by_cost(list_nodes_solution, node_1)
            else: 
                # we explore thurther the branch
                list_nodes_to_explore = node_insert_sort_by_cost(list_nodes_to_explore, node_1)

        
        if node_2.cost <= global_upper_bound:
            if node_2.is_a_leaf():
                # stop branch exploration case, we find a solution
                list_nodes_solution = node_insert_sort_by_cost(list_nodes_solution, node_2)
            else:
                # we explore thurther the branch
                list_nodes_to_explore = node_insert_sort_by_cost(list_nodes_to_explore, node_2)

        # if we change the upper bound, we check that all our nodes to explore are valid
        # otherwise we kill them (i.e the solution of the branch is inferrior as the one found, thus there is no need to continue the exploration)
        if update_upper_bound:
            list_nodes_to_explore = update_node_validity(list_nodes_to_explore, global_upper_bound)


    # So know we look for our solutions nodes
    # First we check their validity, because the global_upper_bound could change since they have been adde to the solution list
    list_nodes_solution = update_node_validity(list_nodes_solution, global_upper_bound)

    # The First node of the list if the solution
    solution_node = list_nodes_solution.pop(0)
    solution_weight = solution_node.total_weight
    solution_value = solution_node.total_value

    # we compute the solution as an array of 0 and 1 that indicate if item i
    # is include or not in the knapsack (given the unmodified list given at the beginning)
    items_in_the_knapsack = []
    while solution_node.parent_node :
        if solution_node.item_selected:
            # we add the item into the knapsack
            items_in_the_knapsack += [Node.items[solution_node.item_index]]

        # we go up on the solution branch
        solution_node = solution_node.parent_node


    # Convert the solution according to the unmodified list of items given at the beginnig.
    answer = [(solution_value, solution_weight)] + format_solution_into_array(items_in_the_knapsack, param_items)
    return answer


# To test the program individually
# with 1 parameter the path to the file containing the knapsack problem
if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(branch_and_bound)