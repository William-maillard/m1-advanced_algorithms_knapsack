# Install dependencies

```bash
> pip install -r requirements.txt
```

## GENERATOR

see the help messages:
 > python3 generator.py    -h

 Mandatory arguments:

 1. file name to save the knapsack problem in the directory /problem
 2. The weight of the knapsack
 3. The number of items to generate

Optionnal arguments for choosing distribution for values(resp weight) of items:

- \-\-values (resp \-\-weight)

    1. SMALL_VALUES
    2. HIGHT_VALUES
    3. EVEN_VALUES

For loading a problem into your program use the module 'knapsack_problem_loader'.
You give it an existing path_file_problem (the problem need to respect the format of the generator)
Then you get the tuple (knapsack_max_weight, [nb_items, (v1, w1), ..., (vn, wn)]).

## LAUNCH individualy an approach

In a more simple way just ad those lines at the end of your file:
(think of replacing brute_force by the name of your function)

```python
# To test the program individually
# with 1 parameter the path to the file containing the knapsack problem
if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(brute_force)
```

Then you can test your algorithm with

```bash
python3 brute_force.py problems/file2.kpbm
```

(think of replacing brute_force by the name of your python file and give
an existing file as an argument)

## LAUNCH script bash

```bash
bash scripts/launch.bash nom_approach1 ... nom_approacheN
```

To select the directory of problem on which to launch files see the file launch.bash and change
the value of the variable dir (comment the current one and decomment the one you want).

## REPORT

See the informations below in the report :

- Modelisation of the problem
- Data Bases used for testing
- Descriptions of each approach

  - Brute-force
  - The approximation based on the minimum spanning tree
  - The three greedy approaches
  - Dynamic programming (+ top-down version)
  - Fully polynomial time sheme
  - Randomized approach
  - Genetic proggramming OR ant colony

## Data set links

set 1 :
> [http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/](http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/) :
