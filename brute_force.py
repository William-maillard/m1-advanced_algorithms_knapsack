""" This module implement a brute force
    approach for solving a 0/1 knapsack
    problem.

    Author: William 
"""

VALUE_INDEX = 0
WEIGHT_INDEX = 1

def brute_force(max_weight, items) -> list :
    """ Brute force approach to solve a knapsack problem.

        Parameters:
        -----------
        max_weight: int
            The maximum weight the knapsack can hold.

        items: list
            [n, (v1, w1), (v2, w2), ..., (vn, wn)] with:
                - n the number of items
                - vi the value of the i° item
                - wi the weight of the i° item


        Returns:
        --------
        knapsack: list
            [(V, W), i1, i2, ..., iN] with:
                - V the knapsack's value
                - W the knapsack's weight
                - ij = 1 if the item is in the knapsack, 0 otherwise
    """

    # The first solution is the empty knapsack
    optimal_solution = [0 for i in range(1, items[0]+1)]
    optimal_solution = [(0, 0)] + optimal_solution
    current_knapsack = optimal_solution.copy()

    # Know we do a binary addition of 1 item from the left
    # until we have browsed every solution.
    # Example:
    # 000, 100, 010, 110, 001, 101, 011, 111
    keep_going = True
    while keep_going :

        # init
        find_new_solution, i = False, 1
        update = (0, 0) # value and weight
        
        # build the next solution

        while not(find_new_solution) and i <= items[0]:

            if current_knapsack[i] == 1 : 
                current_knapsack[i] = 0
                update = (-items[i][0], -items[i][1])
                i += 1
            else:
                current_knapsack[i] = 1
                update = items[i]
                find_new_solution = True

            # remove  or add an item, thus update the knapsack weight and value
            current_knapsack[0] = (current_knapsack[0][0]+update[0], current_knapsack[0][1]+update[1])

        #print(current_knapsack)

        # there is no more solutions to explore
        if not find_new_solution:
            keep_going = False

        # is the new solution valid and better ?
        elif(current_knapsack[0][WEIGHT_INDEX] <= max_weight and
             current_knapsack[0][VALUE_INDEX] > optimal_solution[0][VALUE_INDEX]
             ):
            optimal_solution = current_knapsack.copy()
            #print(optimal_solution)
       

    return optimal_solution



# To test the program individually
# with 1 parameter the path to the file containing the knapsack problem
if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(brute_force)