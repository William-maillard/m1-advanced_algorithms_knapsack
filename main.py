""" @deprecated
    This module contain the function that parse the knapsack problem
    made by the generator and call each solver method, and calculate
    their executing time.


    NB: it is more interesting to be able to launch each algorithm separetly,
    which as been done with 'utilities.individual_approach_test.py' that contains
    a function that parse the command line, load the knapsack problem from the given
    file, call the function pass as an argument and then save the results in the given file

    Author: William 
"""
import argparse
from os.path import isfile
import time
from utilities.knapsack_problem_loader import load_knapsack_problem_from_file
from brute_force import *
from utilities.time import print_time
    

def get_parse_args() -> argparse.Namespace:
    """
        Defines the options availibles on the command line:
        >python3 generator.py W N -v 1 -vp 2.1 -w 1 -wp 2.1

        Mandatory arguments:
        --------------------
            knapsack_problem_file: String
                The path of the file containing the problem to solve.
            result_file: String
                The path to the file in which the results are saved.

        Optionnal arguments:
        --------------------


        return: 
        -------
            args :dict
                key: name of a parameter
                value: value of a parameter
    """
    parser = argparse.ArgumentParser(
        prog="knapsack problems solver",
        description="Solve the knapsack problem in the given file, with each approach, and mesure their execution times in a file",
        epilog="Advanced Algorithm Project 2022"
    )
    parser.add_argument('knapsack_problem_file',
        type=str,
        help="The name of the file in which the knapsack problem is."
    )
    parser.add_argument('result_file',
        type=str,
        help="The name of the file in which results are saved."
    )

    return parser.parse_args()

def main():
    """
        Argument:
        --------
        file containning the knapsack problem.
        
        File format:
        -------------
        W N
        V1 W1
        V2 W2
        ... ...
        VN WN
    """
    args = get_parse_args()

    print("Call each approach separatly with >python3 nom_approach.py fichier_knapsack_pbm -f fichier solution")

    # check if a correct file has been given
    path = args.knapsack_problem_file
    if not isfile(path):
        print("The file path '"+path+"' is not valid. Check it and try again.")
        return -1;

    # extrat the problem from a file
    W, items = load_knapsack_problem_from_file(path)

    print(str(W)+" "+str(items))

    # call each algorithms and mesure their time of execution.
    start = time.perf_counter_ns()
    solution = brute_force(W, items)
    t = time.perf_counter_ns() - start
    print("solution: "+str(solution))
    print(t)
    print_time(t)
    

if __name__ == "__main__":
    main()