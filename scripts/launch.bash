#!/usr/bin/env bash
#Author: William 

# TODO: select a directory:
# decomment one line to chose the set of problem
# you want to launch algorithm on
# can be improve if we take the path as parameter

# External data set
#dir="external_data_set/set_1/low-dimensional"
dir="external_data_set/set_1/large_scale"

# generated problem
#dir="problems/uncorrelated/even_values_even_weights"
#dir="problems/uncorrelated/even_values_hight_weights"
#dir="problems/uncorrelated/even_values_small_weights"
#dir="problems/uncorrelated/hight_values_even_weights"
#dir="problems/uncorrelated/hight_values_hight_weights"
#dir="problems/uncorrelated/hight_values_small_weights"
#dir="problems/uncorrelated/small_values_even_weights"
#dir="problems/uncorrelated/small_values_hight_weights"
#dir="problems/uncorrelated/small_values_small_weights"


files=$(ls $dir)
echo $files

# ----- Get scripts to execute from the command line ----- #

# scripts=$(ls *.py)
# To get the list of arguments command line
# -> list of the algo we want to run.
scripts=$@
echo $scripts
# brute_force.py # maybe special case because take long time

# Creation of results files
dir_results="results/"$dir


# ----- Execute scripts ------ #

# For all script we running problems of the data set
for script in $scripts
do 
    # create a sub-folder for each approach
    approach=$(echo $script | cut -f 1 -d '.')
    echo $approach
 
    # we create the result directory if it not exist
    if [[ ! -d $dir_results/$approach ]]
    then
        mkdir -p $dir_results/$approach
    fi
    # Take care if the dir is not empty, it will override the existing files

    for file in $files
    do
        python3 $script $dir/$file -f $dir_results/$approach/$file
    done

done