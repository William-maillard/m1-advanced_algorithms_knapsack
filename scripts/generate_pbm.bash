#!/usr/bin/env bash
#Author William

# variables from the generator
max_nb_items=1000
max_knapsack_weight=500000


root_dir="./problems"
relation="uncorrelated" # correlated
values_types="small_values hight_values even_values"
weight_types="small_weights hight_weights even_weights"

for r in $relation
do
    i=1 # parameter distribution of the values

    for type in $values_types
    do
        echo $dir

        j=1 #distribution of the weights
        for w_type in $weight_types
        do
            dir=$root_dir"/"$relation"/"$type"_"$w_type"/"

            # we create the result directory if it not exist
            if [[ ! -d $dir ]]
            then
                mkdir -p $dir
            fi
            # Take care if the dir is not empty, it will override the existing files


            # Genrate files with growing nb items  (x7)
            items=10
            while (( items <= $max_nb_items ))
            do

                # problem with weight increasing (x15)
                weight=25
                while (( $weight <= $max_knapsack_weight ))
                do
                    file=$dir"gen_knap_pbm_"$i"_"$j"_"$items"_"$weight".kpbm"
                    python3 generator.py $file  $weight $items -v $i -w $j

                    weight=$(( $weight * 2 ))
                done

                items=$(( $items *2 ))
            done

            j=$(( $j + 1 )) # we change the weight distribution

        done

        i=$(( $i + 1 )) # we change the value distribution

    done
done