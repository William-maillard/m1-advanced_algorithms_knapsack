"""
In order to have an approximate polynomial time algorithm for
the knapsack problem we need to bound the values of each item
by a number k*n.
We define a factor epsilon e such that the solution found is
optimal * (1-e) (the fluctuation come from the approximation algorithm).

To do so we take the hightest value P of all items and divide it by n,
thus we have the bound value by n.
Now we have to consider  the error margin for the solution (here come epsilon)
Highter is epsilon, smaller is the value boundary thus fastes is the algorithm
but it come for a price because the solution will v=be farther to the optimal one.

Thus we will consider e=0.15, allowing a margin of 15% between the solution and 
the optimal one.

Then, with the values bounded we can run a dynamic algorithm on them to find an
approximate solution in polynomial time.
And because the values belong to a bounded set this will reduce the number of computation
of the dynamic algorithm.

sources: 
-understanding the concept:
    https://en.wikipedia.org/wiki/Knapsack_problem#Fully_polynomial_time_approximation_scheme
-mathematical proof:
    https://docslib.org/doc/7326559/the-knapsack-problem-and-fully-polynomial-time-approximation-schemes-fptas

Author: William 
"""
import importlib
dynamic = importlib.import_module("dynamic_top-down") # I shouldn't had put a dash in the file name...

VALUE_INDEX = 0
WEIGHT_INDEX = 1
EPSILON = 0.15 # percentage of error for the solution with the optimal one.
def fully_polynomial_time_approximation(max_weight, items):
    """
    """
    n = items.pop(0) # get the number of items

    # -- First bound the values using the max value of all items --
    max_value = max(items, key=lambda item : item[VALUE_INDEX])[VALUE_INDEX]
    value_boundary = EPSILON * (max_value / n)
    items_with_values_bounded = [
        (items[i][VALUE_INDEX] / value_boundary, items[i][WEIGHT_INDEX]) 
        for i in range(n)
        ]

    # place the number of items at the beginnig of the list
    items.insert(0, n)
    items_with_values_bounded.insert(0, n)


    # -- Second run the dynamic algorithm with the updated values --
    solution = dynamic.dynamic_top_down(max_weight, items_with_values_bounded)

    # -- Third update the value found, because it contains a sum of valmues divided 
    #    by value_boundary, we need to multiply by it this value --
    total_value, total_weight = solution.pop(0)
    total_value = int(total_value * value_boundary)
    solution.insert(0, (total_value, total_weight))

    return solution

if __name__ == "__main__":
    from utilities.individual_approach_test import solve_knapsack_problem_with_approach

    solve_knapsack_problem_with_approach(fully_polynomial_time_approximation)